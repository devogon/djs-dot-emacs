;;; myinitfile --- stuff here

;;; Commentary:
;; use and check use of: initsplit
;;                       use-package
;;              (see https://github.com/jwiegley/dot-emacs/blob/master/init.el)
;; samples here for what i'm currently changing
;; http://www.mygooglest.com/fni/dot-emacs.html
;; also read:
;; http://www.masteringemacs.org/reading-guide/
;; but...do i need this all in different files?
;; read http://milkbox.net/note/single-file-master-emacs-configuration/
;; also http://zeekat.nl/articles/making-emacs-work-for-me.html



(setq custom-file "~/.config/emacs.d/settings.el")
(load custom-file)



;; use this to enable
(let ((default-directory "~/.config/emacs.d/elpa/"))
      (normal-top-level-add-to-load-path '("."))
      (normal-top-level-add-subdirs-to-load-path))
;; end 

(require 'ert)   ; need this until use-package gets updated
(require 'use-package)

;;; =============================
;;; basic config
;;; =============================

(fset 'yes-or-no-p 'y-or-n-p)
(server-start)
(setq backup-inhibited t)


;;; =============================
;;; enable disabled commands
;;; =============================

(put 'downcase-region 'disabled nil)  ; let downcasing work
(put 'upcase-region 'disabled nil)
(put 'narrow-to-defun 'disabled nil)
(put 'narrow-to-page 'disabled nil)
(put 'narrow-to-region 'disabled nil)

;;; =============================
;;; achievements
;;; =============================

(use-package achievements
  :init
  (progn
    (achievements-mode 1)))

;;; =============================
;;; company
;;; =============================

(use-package company
  :disabled t
  :commands company-mode
  :init
  (progn
    (add-hook 'after-init-hook 'global-company-mode)
    ))

;;; =============================
;;; elpa setup (package.el)
;;; =============================

;;; not necessary anymore? some in settings.el


;;; =============================
;;; encryption
;;; =============================

(use-package epa-file)


;;; =============================
;;; ido-mode
;;; =============================

(use-package ido
  :init
  (progn
    (ido-mode 1)))
(ido-mode t)  


;;; =============================
;;; keyfreq
;;; =============================

(use-package keyfreq
  :init
  (progn
  (require 'keyfreq)
  (keyfreq-mode 1)
  (keyfreq-autosave-mode 1)))


;;; =============================
;;; mu4e (email)
;;; =============================

(use-package mu4e
  :load-path "~/.local/share/emacs/site-lisp/mu4e"
  :init
  (progn
    (setq
     mu4e-maildir       "~/Maildir"   ;; top-level Maildir
     mu4e-sent-folder   "/sent"       ;; folder for sent messages
     mu4e-drafts-folder "/drafts"     ;; unfinished messages
     mu4e-trash-folder  "/trash"      ;; trashed messages
     mu4e-refile-folder "/archive"   ;; saved messages
     mail-user-agent 'mu4e-user-agent
     mu4e-update-interval 300
     mu4e-get-mail-command "true"
     mu4e-use-fancy-chars t
     message-kill-buffer-on-exit t
;    mu4e-get-mail-command "/home/ack/bin/djgetmail.sh"
;    mu4e-get-mail-command "offlineimap"
     mu4e-html2text-command "html2text -utf8 -width 72"
     mu4e-attachment-dir "~/mail-attachments")))


;;; =============================
;;; org mode
;;; =============================

(use-package dj-org
  :load-path "~/.config/emacs.d/"
  :bind (("C-c l"  . org-store-link)
	 ("<f6>"   . org-capture)
	 ("C-c a"  . org-agenda)
	 ("C-c b"  . org-iswitchb)
	 ("<f9> o" .  org-remember)
	 ("<f9> r" . remember))
;    (bind-key "M-m"    org-smart-capture)
;    (bind-key "M-M"    org-inline-note)
;    (bind-key "C-c a"  org-agenda)
;    (bind-key "C-c S"  org-store-link)
;    (bind-key "C-c l"  org-insert-link))
;    (bind-key "<f9> R"  remember-region)
  :config
  (progn
    (use-package org-contacts
      :load-path "~/src/org-mode/contrib/lisp/"))
)


;;; =============================
;;; projectile
;;; =============================

(use-package projectile
  :disabled t
  :diminish projectile-mode
  :init
  (projectile-global-mode))

;;; =============================
;;; worklog
;;; =============================

(use-package worklog
  :disabled t
  :init
  (progn
    (setq worklog-automatic-login t)
    (add-hook 'emacs-startup-hook
	      (function (lambda ()
			  (worklog-do-task "hacking emacs" t))))
    (add-hook 'kill-emacs-hook
	      (function (lambda ()
			  (worklog-do-task "logout" t)
			  (worklog-finish))))))

;;; =============================
;;; message to indicate that all has loaded
;;; =============================

(message "yep, got to the end")
