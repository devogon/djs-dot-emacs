;;; customize settings

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-names-vector ["#ded6c5" "#f71010" "#028902" "#ef8300" "#0000ff" "#a020f0" "#528d8d" "#262626"])
 '(ansi-term-color-vector [unspecified "#393939" "#f2777a" "#99cc99" "#ffcc66" "#6699cc" "#cc99cc" "#6699cc" "#e8e6df"] t)
 '(auto-save-default nil)
 '(backup-directory-alist (quote (("" . "~/.cache/emacs.d/emacs-backup"))))
 '(custom-enabled-themes (quote (tsdh-dark)))
 '(custom-safe-themes t)
 '(custom-theme-load-path (quote ("/home/ack/.emacs.d/elpa/alect-themes-20140123.146/" "/home/ack/.emacs.d/elpa/ample-zen-theme-20131129.440/" "/home/ack/.emacs.d/elpa/base16-theme-20130413.1655/" "/home/ack/.emacs.d/elpa/bubbleberry-theme-20131223.349/" "/home/ack/.emacs.d/elpa/clues-theme-20130908.801/" "/home/ack/.emacs.d/elpa/flatland-theme-20131204.858/" "/home/ack/.emacs.d/elpa/leuven-theme-20140113.328/" "/home/ack/.emacs.d/elpa/purple-haze-theme-20130930.1051/" "/home/ack/.emacs.d/elpa/soft-morning-theme-20131211.1342/" "/home/ack/.emacs.d/elpa/soft-stone-theme-20140108.456/" "/home/ack/.emacs.d/elpa/sublime-themes-20140117.323/" custom-theme-directory t)))
 '(display-battery-mode t)
 '(display-time-24hr-format t)
 '(display-time-mode t)
 '(fci-rule-color "#f6f0e1")
 '(font-use-system-font t)
 '(fringe-mode 10 nil (fringe))
 '(geiser-default-implementation "racket")
 '(gnus-logo-colors (quote ("#0d7b72" "#bdbdbd")) t)
 '(gnus-mode-line-image-cache (quote (image :type xpm :ascent center :data "/* XPM */
static char *gnus-pointer[] = {
/* width height num_colors chars_per_pixel */
\"    18    13        2            1\",
/* colors */
\". c #528d8d\",
\"# c None s None\",
/* pixels */
\"##################\",
\"######..##..######\",
\"#####........#####\",
\"#.##.##..##...####\",
\"#...####.###...##.\",
\"#..###.######.....\",
\"#####.########...#\",
\"###########.######\",
\"####.###.#..######\",
\"######..###.######\",
\"###....####.######\",
\"###..######.######\",
\"###########.######\" };")) t)
 '(ido-enable-flex-matching t)
 '(ido-mode (quote buffer) nil (ido))
 '(indicate-buffer-boundaries (quote left))
 '(indicate-empty-lines t)
 '(linum-format " %7i ")
 '(main-line-color1 "#222232")
 '(main-line-color2 "#333343")
 '(mh-identity-list (quote (("work" ((":signature" . "/home/ack/.config/business-signature") ("Organization" . "ack ack") ("Cc" . "") ("From" . "daniël bosold <daniel@ackack.nl>"))) ("personal" ((":signature" . "/home/ack/.config/signature") ("From" . "daniël bosold <daniel@ackack.eu>"))) ("overig" (("From" . "devogon@gmail.com"))))))
 '(org-agenda-skip-deadline-if-done t)
 '(org-capture-templates (quote (("t" "Todo" entry (file+datetree "~/projects/org/gtd.org" "TASKS") "* TODO %^{Description} %^g %? 
         Added: %U") ("n" "Notes" entry (file+datetree "~/projects/org/notes.org") "* %^{Description} %^g %? 
Added: %U") ("a" "Appointment" entry (file+headline "~/projects/org/gtd.org" "CALENDAR") "* APPT %^{Description} %^g
%?
Added: %U") ("j" "Journal" entry (file+datetree "~/projects/org/journal.org") "* %?
Entered on %U
 %i
 %a") ("c" "Contacts" entry (file "~/projects/org/contacts.org") "** %(org-contacts-template-name)
:PROPERTIES:
:EMAIL: %(org-contacts-template-email)
:END:"))) t)
 '(org-catch-invisible-edits (quote smart))
 '(org-clock-into-drawer "CLOCK")
 '(org-clock-persist (quote history))
 '(org-completion-use-ido t)
 '(org-contacts-files (quote ("/home/ack/projects/org/contacts.org")))
 '(org-drill-optimal-factor-matrix (quote ((1 (1.96 . 3.58) (1.7000000000000002 . 3.44) (2.6 . 4.14)))))
 '(org-export-date-timestamp-format "%Y-%m-%d")
 '(org-journal-dir "~/documents/j/journal/")
 '(org-tags-column -78)
 '(org-tags-match-list-sublevels t)
 '(package-archives (quote (("ELPA" . "http://tromey.com/elpa/") ("gnu" . "http://elpa.gnu.org/packages/") ("melpa" . "http://melpa.milkbox.net/packages/") ("marmalade" . "http://marmalade-repo.org/packages/"))))
 '(package-directory-list (quote ("/usr/share/emacs/24.3/site-lisp/elpa" "/usr/share/emacs/site-lisp/elpa" "/home/ack/.local/share/emacs/site/lisp")))
 '(powerline-color1 "#222232")
 '(powerline-color2 "#333343")
 '(quack-default-program "csi")
 '(quack-smart-open-paren-p t)
 '(scroll-bar-mode nil)
 '(scroll-preserve-screen-position nil)
 '(send-mail-function (quote smtpmail-send-it))
 '(show-paren-mode t)
 '(smtpmail-default-smtp-server "smtp.gmail.com")
 '(smtpmail-smtp-server "smtp.gmail.com")
 '(smtpmail-smtp-service 587)
 '(smtpmail-stream-type (quote starttls))
 '(tool-bar-mode nil)
 '(tooltip-mode nil)
 '(vc-annotate-background "#f6f0e1")
 '(vc-annotate-color-map (quote ((20 . "#e43838") (40 . "#f71010") (60 . "#ab9c3a") (80 . "#9ca30b") (100 . "#ef8300") (120 . "#958323") (140 . "#1c9e28") (160 . "#3cb368") (180 . "#028902") (200 . "#008b45") (220 . "#077707") (240 . "#409e9f") (260 . "#528d8d") (280 . "#1fb3b3") (300 . "#2c53ca") (320 . "#0000ff") (340 . "#0505cc") (360 . "#a020f0"))))
 '(vc-annotate-very-old-color "#a020f0")
 '(visible-bell t)
 '(whois-server-list (quote (("whois.verisign-grs.com") ("whois.eu") ("whois.arin.net") ("rs.internic.net") ("whois.publicinterestregistry.net") ("whois.abuse.net") ("whois.apnic.net") ("nic.ddn.mil") ("whois.nic.mil") ("whois.nic.gov") ("whois.ripe.net"))))
 '(whois-server-name "whois.domain-registry.nl")
 '(whois-server-tld (quote (("whois.eu" . "eu") ("whois.domain-registry.nl" . "nl") ("rs.internic.net" . "com") ("whois.publicinterestregistry.net" . "org") ("whois.ripe.net" . "be") ("whois.ripe.net" . "de") ("whois.ripe.net" . "dk") ("whois.ripe.net" . "it") ("whois.ripe.net" . "fi") ("whois.ripe.net" . "fr") ("whois.ripe.net" . "uk") ("whois.apnic.net" . "au") ("whois.apnic.net" . "ch") ("whois.apnic.net" . "hk") ("whois.apnic.net" . "jp") ("whois.nic.gov" . "gov") ("whois.nic.mil" . "mil")))))
;; (custom-set-faces
;;  ;; custom-set-faces was added by Custom.
;;  ;; If you edit it by hand, you could mess it up, so be careful.
;;  ;; Your init file should contain only one such instance.
;;  ;; If there is more than one, they won't work right.
;;  '(default ((t (:inherit nil :stipple nil :background "#102e4e" :foreground "#eeeeee" :inverse-video nil :box nil :strike-through nil :overline nil :underline nil :slant normal :weight normal :height 90 :width normal :foundry "unknown" :family "DejaVu Sans Mono")))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:family "Bitstream Vera Sans Mono" :foundry "bitstream" :slant normal :weight normal :height 98 :width normal)))))
