(provide 'dj-org)

;(when section-org (message "number org...")

;=============================
; org-mode
;=============================
(require 'org)
(require 'org-agenda)
;(require 'org-smart-capture)

;(setq load-path (cons "~/src/org-mode/lisp" load-path))
;(setq load-path (cons "~/src/org-mode/contrib/lisp" load-path))

;; read these (charles cave)
;; http://members.optusnet.com.au/~charles57/GTD/gtd_workflow.html

(setq org-modules
      '(org-bbdb
	org-info
	org-jsinfo
	org-irc
	org-w3m
	org-id
	org-habit))

(require 'org-habit)
(setq org-directory "~/projects/org")
(setq org-default-notes-file (concat org-directory "/notes.org"))


(setq calendar-date-style 'european)
;; set timestamp when item is marked done
(setq org-log-done t)
(setq org-log-done (quote time))

(setq org-clock-into-drawer "CLOCK")

(setq org-clock-persist 'history)
(org-clock-persistence-insinuate)

(setq org-deadline-warning-days 14)
(setq org-agenda-show-all-dates t)
(setq org-agenda-skip-deadline-if-done t)
(setq org-agenda-skip-scheduled-if-done t)
(setq org-agenda-start-on-weekday nil)
(setq org-timeline-show-empty-dates t)
(setq org-insert-mode-line-in-empty-file t)
(setq org-tags-column -78)
(setq org-tags-match-list-sublevels nil)

(setq org-hide-leading-stars t)
(setq org-agenda-include-all-todo t)


(add-to-list 'auto-mode-alist '("\\.\\(org\\|org_archive\\|txt\\)$" . org-mode))
(add-hook 'org-mode-hook 'turn-on-font-lock)


(setq org-capture-templates
      '(("t" "Todo" entry (file+datetree "~/projects/org/gtd.org" "TASKS")
	 "* TODO %^{Description} %^g %? 
         Added: %U")
	("n" "Notes" entry (file+datetree "~/projects/org/notes.org") 
	 "* %^{Description} %^g %? 
Added: %U") 
	("a" "Appointment" entry (file+headline
				  "~/projects/org/gtd.org" "CALENDAR")
	 "* APPT %^{Description} %^g
%?
Added: %U")
	("j" "Journal" entry (file+datetree "~/projects/org/journal.org")
	     "* %?\nEntered on %U\n %i\n %a")
	("c" "Contacts" entry (file "~/documents/a/addressbook/contacts.org")
	 "** %(org-contacts-template-name)
:PROPERTIES:
:EMAIL: %(org-contacts-template-email)
:END:")))


;----------
; project files
;----------

(setq org-agenda-files (list "~/projects/org/organiser.org"
			     "~/projects/org/refile.org"
			     "~/projects/org/todo.org"
			     "~/projects/org/gtd.org"
;			     "~/documents/a/addressbook/contacts.org"
			     "~/projects/org/ewald.org"
			     "~/projects/org/notes.org"
			     "~/projects/org/journal.org"
;			     "~/projects/fietswinkel/org/winkelnotes.org"
			     "~/projects/org/anniv.org"
;			     "~/projects/fietswinkel/org/todo.org"
;			     "~/personal/org/fietsen.org"
))



(setq org-agenda-include-diary nil)


(require 'org-publish)

(setq org-cycle-include-plain-lists t)

(setq org-directory "~/projects/org")
(setq org-default-notes-file (concat org-directory "/refile.org"))


(setq org-refile-targets (quote (("organiser.org" :maxlevel . 1) ("someday.org" :level . 2))))

; targets include this file and any file contributing to the agenda
(setq org-refile-targets (quote ((org-agenda-files :maxlevel . 5) (nil :maxlevel . 5))))

; targets start with the file name - allows creating level 1 tasks
(setq org-refile-use-outline-path (quote file))

; targets complete in steps so we start with filename, tab shows the next level of targets etc
(setq org-outline-path-complete-in-steps t)

; allow refile to create parent tasks with confirmation
(setq org-refile-allow-creating-parent-nodes (quote confirm))

;=============================
; mobile org
;=============================

;(setq org-mobile-directory "~/Dropbox/org")
(setq org-mobile-inbox-for-pull "~/projects/org/from-mobile.org")
(setq org-mobile-files (quote ("gtd.org")))

;; ek 2012 file
;(add-to-list 'org-agenda-files "~/src/org-euro2012/euro2012.org")


(message "org mode... ... done")
